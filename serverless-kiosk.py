"""
A serverless kiosk which runs on AWS Lambda + API Gateway and gets its information from DynamoDB
"""
import boto3


# Typing
try:
    from typing import Dict, Optional, TypeVar, Union, Any

    T = TypeVar("T")
except ImportError:
    pass


class Request(object):
    """
    Takes an API Gateway request event and turns it into an actual object
    """
    body = None  # type: str
    query_string_paramaters = {}  # type: Dict[str, str]
    http_method = None  # type: str
    headers = {}  # type: Dict[str, str]
    path = None  # type: str

    def __init__(self, event):
        # type: (dict) -> None
        self.body = event.get("body")
        self.query_string_paramaters = event.get("queryStringParameters", {})
        self.http_method = event.get("httpMethod")
        self.headers = event.get("headers", {})
        self.path = event.get("path")  # Empty path is "/"

    def is_root_path(self):
        # type: () -> bool
        # Root path is "/"
        return self.path == "/"


class Response(object):
    """
    A helper for building a response to send back to API Gateway
    """
    STATUS_OK = "200"

    body = None  # type: str
    status_code = None  # type: int
    headers = {}  # type: Dict[str, str]

    def __init__(self, body, status_code=200):
        # type: (str, int) -> None
        self.body = body
        self.status_code = status_code

        # Default content type
        self.headers = {
            'content-type': "text/html",
        }

    def serialise(self):
        # type: () -> dict
        """
        Turns the response object into a dictionary to pass back to API Gateway
        """
        return {
            'statusCode': str(self.status_code),
            'body': self.body,
            'headers': self.headers,
        }


page = """
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Kiosk</title>
        <style type="text/css">
            body, html {{
                margin: 0;
                padding: 0;
                height: 100%;
                overflow: hidden;
            }}

            #content {{
                position:absolute;
                left: 0;
                right: 0;
                bottom: 0;
                top: 0px; 
            }}
        </style>
        <script>
            var urls = [{urls}];
            var i = 0;
            var delay = {delay};

            function updateFrame(url) {{
                document.getElementById('main').src = url;
            }}

            function updateFrameFromIndex(i) {{
                updateFrame(urls[i]);
            }}

            function onLoad() {{
                // Change page every `delay` ms
                setInterval(function() {{
                    updateFrameFromIndex(i);
                    i++;
                    if (i >= urls.length) {{
                        i = 0;
                    }}
                }}, delay);

                // Refresh the page every hour to clear memory leaks
                setTimeout(function() {{
                    location.reload();
                }}, 60 * 60 * 1000);

                // Load first page
                updateFrameFromIndex(i);
                i++;
            }}
        </script>
    </head>
    <body onload="onLoad()">
        <div id="content">
            <iframe id="main" width="100%" height="100%" frameborder="0" src=""></iframe>
        </div>
    </body>
</html>
"""


def lambda_handler(event, context):
    """
    Main entry point for the program.
    Detects path and routes requests
    :param event: API Gateway request
    :param context: ??
    :return: A response object
    """
    request = Request(event)  # type: Request

    # Connect to dynamodb
    dynamo = boto3.resource('dynamodb').Table('kiosk')

    if request.is_root_path():
        # Homepage
        response = Response("Kiosk tool. Missing key", 404)
    else:
        # Kiosk page
        response = kiosk_page_handler(request, dynamo)

    return response.serialise()


def kiosk_page_handler(request, dynamo):
    # type: (Request, Any) -> Response
    """
    Handles a request for a particular post
    """
    query = dynamo.get_item(
        Key={
            'key': request.path[1:]
        }
    )
    record = query.get('Item')
    if not record:
        return Response("Kiosk not found", 404)

    urls = record.get('urls', [])
    delay = record.get('delay', 10000)

    body = page.format(
        urls=','.join('"{}"'.format(url) for url in urls),
        delay=delay,
    )

    return Response(body)
