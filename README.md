A really simple system where you can create a Kiosk which has an iFrame and alternates through the pages.

Gets all its data from dynamodb. Create a table called `kiosk` with the string key `key`, each record should have a number of ms `delay` and a set of strings `urls` which should be displayed.

## Limitations
Sites can set a header which stops the browser loading their pages in an iFrame.

If you really need to get around this, you can open it in Chrome with security disabled:
```
chromium-browser --disable-web-security --user-data-dir
```
